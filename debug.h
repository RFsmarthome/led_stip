/*
 * debug.h
 *
 *  Created on: 08.04.2016
 *      Author: schnake
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#define DEBUG_INIT DDRB |= _BV(PB0);
#define DEBUG_ON PORTB |= _BV(PB0);
#define DEBUG_OFF PORTB &= ~_BV(PB0);

#endif /* DEBUG_H_ */
