#include <stdint.h>

#include <avr/eeprom.h>
#include <util/delay.h>

#include "packet.h"
#include "crc8.h"
#include "lt8900.h"
#include "xtea.h"
#include "debug.h"

bool packet_send(uint8_t address, struct buspacket_t *packet, bool wait)
{
	packet->packet = 0;
	packet->ack = 0;
	packet->req_ack = 0;

	uint8_t crc = crc_message(CRC_POLYNOM, (uint8_t*)packet, sizeof(struct buspacket_t) - 1);
	packet->crc8 = crc;

	bool rc = lt8900_send(address, packet, sizeof(struct buspacket_t), wait);
	return rc;
}
