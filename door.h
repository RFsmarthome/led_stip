/*
 * door.h
 *
 *  Created on: 08.04.2016
 *      Author: schnake
 */

#ifndef DOOR_H_
#define DOOR_H_

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>

extern void door_init(bool pullup);
extern bool door_status();

#endif /* DOOR_H_ */
