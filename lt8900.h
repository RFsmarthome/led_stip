/*
 * lt8900.h
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#ifndef LT8900_H_
#define LT8900_H_

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>

struct buspacket_t;

#define SYNC_WORD 0x0123456789abcdef

extern void lt8900_init(uint8_t retransmit, uint8_t channel, bool pkt_pullup);

extern void lt8900_init_hardware();
extern void lt8900_reset();
extern bool lt8900_test();

extern void lt8900_setSyncWord(uint64_t syncword);
extern void lt8900_setAddress(uint8_t address);
extern uint8_t lt8900_getChannel();
extern void lt8900_setChannel(uint8_t channel);
extern void lt8900_setPower(uint8_t power, uint8_t gain);

extern void lt8900_scanRSSI(uint16_t *buffer, uint8_t start_channel, uint8_t num_channels);
extern uint8_t lt8900_getRSSI();

extern void lt8900_listen(uint8_t address);
extern bool lt8900_hasData();
extern void lt8900_clearFifo();

extern int lt8900_receive(void *buffer, int maxBufferSize);
extern bool lt8900_send(uint8_t address, const void *buffer, int length, bool wait);

extern bool lt8900_waitData(uint32_t timeout);

extern void lt8900_sleep();
//extern void lt8900_powerdown();
extern void lt8900_wakeup();

#endif /* LT8900_H_ */
