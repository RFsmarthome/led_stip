#ifndef _PWM_H
#define _PWM_H

#include <avr/io.h>

extern void pwm_init();
extern void pwm_set_dim(uint8_t dimmer);
extern void pwm_set_frequency(uint8_t level);

#endif
