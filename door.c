/*
 * door.c
 *
 *  Created on: 08.04.2016
 *      Author: schnake
 */

#include "door.h"
#include "defines.h"
#include <util/delay.h>

void door_init(bool pullup)
{
	DOOR_DDR &= ~_BV(DOOR_PIN); // PKT pin as input
	if(pullup) {
		DOOR_PORT |= _BV(DOOR_PIN); // PKT Pull up on
	} else {
		DOOR_PORT &= ~_BV(DOOR_PIN); // PKT Pull up off
	}
}

bool door_status()
{
	return (DOOR_SPIN & _BV(DOOR_PIN)) != 0;
}
