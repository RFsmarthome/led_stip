/*
 * pwm.cpp
 *
 * Created: 26.10.2015 17:56:46
 * Author : schnake
 */

#include <stdint.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/cpufunc.h>

#include "schnakebus.h"

#include "defines.h"

#include "packet.h"
#include "pwm.h"
#include "lt8900.h"
#include "crc8.h"
#include "door.h"
#include "xtea.h"

uint8_t rf_address EEMEM = 4;

volatile static uint8_t g_interrupt = 0;

#define INT_LT8900 1
#define INT_DOOR 2
#define INT_TIMER 3

uint8_t mcusr_mirror __attribute__ ((section (".noinit")));
void get_mcusr(void) \
  __attribute__((naked)) \
  __attribute__((section(".init3")));
void get_mcusr(void)
{
	mcusr_mirror = MCUSR;
	MCUSR = 0;
	wdt_disable();
}

void sendDoorStatus(uint8_t my_address, struct buspacket_t *packet)
{
	packet->version = SB_VERSION;
	packet->type = TYPE_ON_OFF;
	packet->source = my_address;
	if(door_status()) {
		packet->onoff.onoff = 1;
	} else {
		packet->onoff.onoff = 0;
	}
	packet_send(0, packet, true);
}

void clearInterrupt()
{
	EIFR = _BV(INTF1)| _BV(INTF0);
	g_interrupt = 0;
}

void timer_init()
{
	TIMSK2 = 0;

	TCCR2A = _BV(WGM21);
	TCCR2B = _BV(CS22) | _BV(CS21) | _BV(CS20); // 1024
	OCR2A = 255;

	TIFR2 = _BV(TOV2) | _BV(OCF2A);
	TIMSK2 = _BV(OCIE2A);
}

int main(void)
{
	cli();
	wdt_enable(WDTO_2S);
	wdt_reset();

	DDRB = 0x00;
	PORTB = 0xff;
	DDRC = 0x00;
	PORTC = 0xff;
	DDRD = 0x00;
	PORTD = 0xff;

	power_all_disable();
	power_timer0_enable();
	pwm_init();

	door_init(false);

	power_spi_enable();
	lt8900_init(7, 120, false);

	wdt_reset();

	lt8900_init_hardware();
	lt8900_setPower(15, 4);

	if(!lt8900_test()) {
		/* Test no okay, so wait for watchdog */
		for(;;);
	}

	uint8_t my_address = eeprom_read_byte(&rf_address);
	struct buspacket_t packet;

	// Enable external interrupt
	EICRA = _BV(ISC11) | _BV(ISC10) | _BV(ISC01);
	EIMSK = _BV(INT0) | _BV(INT1);
	//EIMSK = _BV(INT1);

	// Send door status
	//sendDoorStatus(my_address, &packet);
	//lt8900_clearFifo();

	// Begin listen
	lt8900_listen(my_address);

	uint16_t countCycles = -1;
	bool oldDoorStatus = door_status();

	power_timer2_enable();
	timer_init();

	clearInterrupt();
	sei();

	while(1) {
		wdt_reset();

		// Send door status by change or after time
		if((oldDoorStatus != door_status()) || (countCycles++>250)) {
			cli();
			countCycles = 0;

			sendDoorStatus(my_address, &packet);
			lt8900_clearFifo();

			lt8900_listen(my_address);
			clearInterrupt();
			sei();
		}

		switch(g_interrupt) {
		case INT_LT8900:
			cli();
			if(lt8900_hasData()) {
				uint8_t length = lt8900_receive(&packet, sizeof(packet));
				if(packet.version == SB_VERSION) {
					// My address
					//uint8_t crc = crc_messagecalc(&packet, length - sizeof(uint8_t));
					if(crc_message(CRC_POLYNOM, (uint8_t*)&packet, length)==0) {
						// CRC8 seems okay
						uint8_t dimmer = 100;
						uint8_t on_off = 1;

						switch(packet.type) {
						case TYPE_DIMMER:
							// Package type DIMMER
							dimmer = packet.dimmer.dimmer;
							break;
						case TYPE_ON_OFF:
							on_off = packet.onoff.onoff;
							break;
						default:
							break;
						}

						if(on_off) {
							pwm_set_dim(dimmer);
						} else {
							pwm_set_dim(0);
						}
					}
				}
			}
			lt8900_listen(my_address);
			clearInterrupt();
			sei();
			break;
		case INT_TIMER:
			break;
		}

		cli();
		clearInterrupt();

		set_sleep_mode(SLEEP_MODE_IDLE);
		sei();
		sleep_mode();
	}
}

/* Door status */
ISR(INT0_vect)
{
	if(!g_interrupt) g_interrupt = INT_DOOR;
}

/* Packet received */
ISR(INT1_vect)
{
	if(!g_interrupt) g_interrupt = INT_LT8900;
}


/* TIMER 2 wakeup */
ISR(TIMER2_COMPA_vect)
{
	if(!g_interrupt) g_interrupt = INT_TIMER;
}
