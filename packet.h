#ifndef _PACKET_H
#define _PACKET_H

#include <stdbool.h>

#include "schnakebus.h"

bool packet_send(uint8_t address, struct buspacket_t *packet, bool wait);

#endif
