/*
 * defines.h
 *
 *  Created on: 20.11.2015
 *      Author: schnake
 */

#ifndef DEFINES_H_
#define DEFINES_H_

/*
 * LT89000
 */
#define CS_DDR DDRB
#define CS_PORT PORTB
#define CS_PIN PB2

#define RST_DDR DDRD
#define RST_PORT PORTD
#define RST_PIN PD4

#define PKT_DDR DDRD
#define PKT_PORT PORTD
#define PKT_SPIN PIND
#define PKT_PIN PD3

/*
 * Door Switch
 */
#define DOOR_DDR DDRD
#define DOOR_PORT PORTD
#define DOOR_SPIN PIND
#define DOOR_PIN PD2

#endif /* DEFINES_H_ */
